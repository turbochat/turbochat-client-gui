# Turbochat-PROTOTYPE

### Dependencies:

 - C Compiler
 - CMake
 - Rust (Rustup is recommended)
 - Cargo

### Building:

Initialize the turbochat-client-backend submodule before the first build:
`git submodule update --init`

`mkdir build && cd build`
    
`cmake ..`
   
`make -j [NUMBER OF THREADS]` -> Faster
   OR
`cmake --build .` -> Slower

<br>
=> The binary file will be located in the build directory.
<br>

### To clean the build:

`make clean`
