#include <iostream>

#include "libcall.hpp"


void Libcall::testButton()
{
    emit addContactCard(0, false, 0, "User", "resources/testPP");
}

void Libcall::sendMessage(QString message)
{
    std::cout << "Send message: Not implemented yet" << std::endl;
    emit addMessageBubble(0, 0, "User", message, true);
}

void Libcall::changeActiveChat(QString contactID)
{
    std::cout << "Change active chat: Not implemented yet" << std::endl;
}

void Libcall::contactCardCallback()
{

}
