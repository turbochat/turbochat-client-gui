import QtQuick

Rectangle {
    property string id: ""
    property string contactId: ""
    property string contactName: ""
    property string contactPictureSource: ""
    
    
    id: contactCardContainer
    width: 240
    height: 55
    color: "#2a2a2a"
    radius: 6
    border.width: 0
    anchors.horizontalCenter: parent.horizontalCenter
    
    Text {
        id: contactCardUserName
        y: 8
        color: "#adadad"
        text: contactName
        anchors.left: contactCardPicture.right
        anchors.leftMargin: 7
        font.family: "Tahoma"
    }
    Image {
        id: contactCardPicture
        width: 38
        height: 40
        asynchronous: true
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        source: contactPictureSource
        fillMode: Image.PreserveAspectCrop
        anchors.leftMargin: 8
    }
    Text {
        id: contactCardId
        y: 30
        color: "#adadad"
        text: contactId
        anchors.left: contactCardPicture.right
        anchors.leftMargin: 7
        font.family: "Tahoma"
    }
    MouseArea {
        id: cardMouseArea
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor
        
        Connections {
            target: cardMouseArea
            function onClicked() {
                libcall.changeActiveChat(contactId);
            }
            function onEntered() {
                contactCardContainer.color = "#353535"
            }
            function onExited() {
                contactCardContainer.color = "#2a2a2a"
            }
        }
    }
}
