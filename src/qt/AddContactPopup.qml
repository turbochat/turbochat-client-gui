import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs

Popup {
    id: popup
    anchors.centerIn: Overlay.overlay
    width: 700
    height: 220
    modal: true
    focus: true
    padding: 1
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    background: Rectangle {
        id: background
        color: "#505050"
        border.color: "#adadad"
        border.width: 1
    }
    Overlay.modal: Rectangle {
        color: "#96000000"
    }
    
    Buttons.TopBarButton {
        id: addServerButton
        width: parent.width/2
        anchors.left: parent.left
        height: 40
        buttonText: "Add Server"
        color: "#2a2a2a"
        onClicked: () => {
            addPrivateChatButton.color = "#2a2a2a";
            addContactSwipeView.setCurrentIndex(0);
        }
    }
    Buttons.TopBarButton {
        id: addPrivateChatButton
        width: parent.width/2
        anchors.left: addServerButton.right
        height: 40
        buttonText: "Add Private Chat"
        color: "#2a2a2a"
        onClicked: () => {
            addServerButton.color = "#2a2a2a";
            addContactSwipeView.setCurrentIndex(1);
        }
    }

    SwipeView {
        id: addContactSwipeView
        currentIndex: 2
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: addPrivateChatButton.bottom
        anchors.bottom: parent.bottom
        interactive: false
        clip: true

        Item {
            id: serverMenuContainer
            width: background.width
            height: background.height

            TextField {
                id: serverNameField
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: serverPictureContainer.left
                anchors.leftMargin: 20
                anchors.topMargin: 20
                anchors.rightMargin: 60
                color: "#b3b3b3"
                background: Rectangle {
                    color: "#2a2a2a"
                }
                placeholderText: "Name@Id:"
                placeholderTextColor: "#787878"
                maximumLength: 300

                Connections {
                    target: serverNameField
                    function onTextChanged() {
                        //TODO: Backend: Wait for ~2 sec before doing the query (counter which resets with every onTextChanged)
                    }
                }
            }

            Rectangle {
                id: serverPictureContainer
                color: "#505050"
                border.width: 1
                border.color: "#b3b3b3"
                radius: 6
                height: 20+(2*serverNameField.height)
                width: height
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.rightMargin: 60

                Image {
                    id: serverPicture
                    source: "resources/icon.png"
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectCrop
                    anchors.margins: 2
                }
                Rectangle {
                    id: changeServerImageMask
                    anchors.fill: parent
                    color: "#320f0f0f"
                    radius: 6
                    visible: false
                }
                Text {
                    id: changeServerImage
                    text: "Change\nImage"
                    color: "#b3b3b3"
                    visible: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                }

                MouseArea {
                    id: serverPictureMouseArea
                    hoverEnabled: true
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor

                    Connections {
                        target: serverPictureMouseArea
                        function onClicked() {
                            //TODO: Open dialog for choosing Image
                            console.log("Open file selector dialogue: Not implemented yet")
                        }
                        function onEntered() {
                            changeServerImage.visible=true
                            changeServerImageMask.visible=true
                        }
                        function onExited() {
                            changeServerImage.visible=false
                            changeServerImageMask.visible=false
                        }
                    }
                }
            }
            TextField {
                id: serverPreferredNameField
                anchors.left: parent.left
                anchors.top: serverNameField.bottom
                anchors.right: serverPictureContainer.left
                anchors.leftMargin: 20
                anchors.topMargin: 20
                anchors.rightMargin: 60
                color: "#b3b3b3"
                background: Rectangle {
                    color: "#2a2a2a"
                }
                placeholderText: "Preferred Name:"
                placeholderTextColor: "#787878"
            }
            Buttons.BorderedButton {
                id: serverButtonCancel
                width: 70
                height: 30
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.top: serverPreferredNameField.bottom
                anchors.topMargin: 20
                buttonText: "Cancel"
                onClicked: () => {
                    serverNameField.clear();
                    serverPreferredNameField.clear();
                    close();
                }
            }
            Buttons.BorderedButton {
                id: serverButtonOk
                width: 70
                height: 30
                anchors.left: serverButtonCancel.right
                anchors.leftMargin: 20
                anchors.top: serverPreferredNameField.bottom
                anchors.topMargin: 20
                buttonText: "Ok"
                onClicked: () => {
                    //TODO: Add Server to List
                    console.log("Name@Id: " + serverNameField.text + ", Preferred Name: " + serverPreferredNameField.text);
                    serverNameField.clear();
                    serverPreferredNameField.clear();
                    close();
                }
            }
        }
        Item {
            id: privateChatMenuContainer
            width: background.width
            height: background.height

            TextField {
                id: privateChatNameField
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: privateChatPictureContainer.left
                anchors.leftMargin: 20
                anchors.topMargin: 20
                anchors.rightMargin: 60
                color: "#b3b3b3"
                background: Rectangle {
                    color: "#2a2a2a"
                }
                placeholderText: "Name@Id:"
                placeholderTextColor: "#787878"
                maximumLength: 300

                Connections {
                    target: privateChatNameField
                    function onTextChanged() {
                        //TODO: Backend: Wait for ~2 sec before doing the query (counter which resets with every onTextChanged)
                    }
                }

            }

            Rectangle {
                id: privateChatPictureContainer
                color: "#505050"
                border.width: 1
                border.color: "#b3b3b3"
                radius: 6
                height: 20+(2*privateChatNameField.height)
                width: height
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.rightMargin: 60

                Image {
                    id: privateChatPicture
                    source: "resources/icon.png"
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectCrop
                    anchors.margins: 2
                }
                Rectangle {
                    id: changePrivateChatImageMask
                    anchors.fill: parent
                    color: "#320f0f0f"
                    radius: 6
                    visible: false
                }
                Text {
                    id: changePrivateChatImage
                    text: "Change\nImage"
                    color: "#b3b3b3"
                    visible: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                }

                MouseArea {
                    id: privateChatPictureMouseArea
                    hoverEnabled: true
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor

                    Connections {
                        target: privateChatPictureMouseArea
                        function onClicked() {
                            //TODO: Open dialog for choosing Image
                            console.log("Open file selector dialogue: Not implemented yet")
                        }
                        function onEntered() {
                            changePrivateChatImage.visible=true
                            changePrivateChatImageMask.visible=true
                        }
                        function onExited() {
                            changePrivateChatImage.visible=false
                            changePrivateChatImageMask.visible=false
                        }
                    }
                }
            }
            TextField {
                id: privateChatPreferredNameField
                anchors.left: parent.left
                anchors.top: privateChatNameField.bottom
                anchors.right: privateChatPictureContainer.left
                anchors.leftMargin: 20
                anchors.topMargin: 20
                anchors.rightMargin: 60
                color: "#b3b3b3"
                background: Rectangle {
                    color: "#2a2a2a"
                }
                placeholderText: "Preferred Name:"
                placeholderTextColor: "#787878"
            }
            Buttons.BorderedButton {
                id: privateChatButtonCancel
                width: 70
                height: 30
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.top: privateChatPreferredNameField.bottom
                anchors.topMargin: 20
                buttonText: "Cancel"
                onClicked: () => {
                    privateChatNameField.clear();
                    privateChatPreferredNameField.clear();
                    close();
                }
            }
            Buttons.BorderedButton {
                id: privateChatButtonOk
                width: 70
                height: 30
                anchors.left: privateChatButtonCancel.right
                anchors.leftMargin: 20
                anchors.top: privateChatPreferredNameField.bottom
                anchors.topMargin: 20
                buttonText: "Ok"
                onClicked: () => {
                    //TODO: Add Private Chat to List
                    console.log("Name@Id: " + privateChatNameField.text + ", Preferred Name: " + privateChatPreferredNameField.text);
                    privateChatNameField.clear();
                    privateChatPreferredNameField.clear();
                    close();
                }
            }
        }
    }

    function setSelectedMode(isServerSelected) {
        if (isServerSelected) {
            addServerButton.color = "#505050";
            addPrivateChatButton.color = "#2a2a2a";
            addContactSwipeView.setCurrentIndex(0);
        }
        else {
            addServerButton.color = "#2a2a2a";
            addPrivateChatButton.color = "#505050";
            addContactSwipeView.setCurrentIndex(1);
        }
    }
    
    Connections {
        target: popup
        function onAboutToHide () {
            privateChatNameField.clear();
            privateChatPreferredNameField.clear();
            serverNameField.clear();
            serverPreferredNameField.clear();
        }
    }
}
