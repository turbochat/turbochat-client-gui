import QtQuick

Rectangle {
    property string id: ""
    property string contactId: ""
    property string contactName: ""
    property string contactPictureSource: ""
        
    id: contactCardContainer
    width: 240
    height: 55
    color: "#2a2a2a"
    radius: 6
    anchors.horizontalCenter: parent.horizontalCenter
    
    Text {
        id: contactCardServerName
        color: "#adadad"
        text: contactName
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: contactCardPicture.right
        anchors.leftMargin: 7
        font.family: "Tahoma"
    }
    Image {
        id: contactCardPicture
        width: 38
        height: 40
        asynchronous: true
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        source: contactPictureSource
        anchors.leftMargin: 8
        fillMode: Image.PreserveAspectCrop
    }

    MouseArea {
        id: contactCardMouseArea
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor
        
        Connections {
            target: contactCardMouseArea
            function onClicked() {
                libcall.changeActiveChat(contactId);
            }
            function onEntered() {
                contactCardContainer.color = "#353535"
            }
            function onExited() {
                contactCardContainer.color = "#2a2a2a"
            }
        }
    }
}
