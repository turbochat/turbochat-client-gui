import QtQuick
import QtQuick.Controls

Popup {
    id: settingsPopup
    anchors.centerIn: Overlay.overlay
    width: parent.width - (parent.width/100)*20
    height: parent.height - (parent.height/100)*20
    modal: true
    focus: true
    padding: 1
    closePolicy: Popup.CloseOnEscape
    
    onAboutToHide: () => {
        nicknameField.clear()
        registeredIdField.clear()
        registeredServerField.clear()
    }
    
    background: Rectangle {
        id: background
        color: "#505050"
        border.color: "#adadad"
        border.width: 1
    }
    Overlay.modal: Rectangle {
        color: "#96000000"
    }
    Row {
        id: settingButtonRow
        anchors.left: parent.left
        anchors.right: parent.right
        height: 40
        
        Buttons.TopBarButton {
            id: accountButton
            width: parent.width/3
            height: parent.height
            buttonText: "Account Settings"
            color: "#505050"
            
            onClicked: ()=> {
                audioVideoButton.color="#2a2a2a"
                appereanceButton.color="#2a2a2a"
                settingsSwipeView.setCurrentIndex(0)
            }
        }
        Buttons.TopBarButton {
            id: audioVideoButton
            width: parent.width/3
            height: parent.height
            buttonText: "Audio/Video Settings"
            color: "#2a2a2a"
            
            onClicked: ()=> {
                accountButton.color="#2a2a2a"
                appereanceButton.color="#2a2a2a"
                settingsSwipeView.setCurrentIndex(1)
            }
        }
        Buttons.TopBarButton {
            id: appereanceButton
            width: parent.width/3
            height: parent.height
            buttonText: "Appereance Settings"
            color: "#2a2a2a"
            
            onClicked: ()=> {
                audioVideoButton.color="#2a2a2a"
                accountButton.color="#2a2a2a"
                settingsSwipeView.setCurrentIndex(2)
            }
        }
    }
    Rectangle {
        id: settingsFooter
        height: 50
        color: background.color
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        Buttons.BorderedButton {
            id: applyButton
            onClicked: ()=> {
                console.log("Apply new settings: Not implemented yet")  //TODO: Apply new Settings
            }
            width: 70
            height: 30
            buttonText: "Apply"
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 10
        }
        Buttons.BorderedButton {
            id: cancelButton
            onClicked: ()=> {
                settingsPopup.close()
            }
            width: 70
            height: 30
            buttonText: "Cancel"
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: applyButton.left
            anchors.rightMargin: 10
        }
    }
    SwipeView {
        id: settingsSwipeView
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: settingButtonRow.bottom
        anchors.bottom: settingsFooter.top
        currentIndex: 0
        interactive: false
        clip: true
        
        Item {
            id: accountSettingsContainer
            
            Rectangle {
                id: accountDetailsContainer
                width: fieldsColumn.width + profilePicture.width + 40 + settingsPopup.width / 5
                height: fieldsColumn.height
                color: background.color
                border.color: "#b3b3b3"
                border.width: 1
                radius: 6
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: settingsPopup.width / 15
                
                Column {
                    id: fieldsColumn
                    spacing: parent.height / 15
                    padding: parent.height / 10
                    
                    TextField {
                        id: nicknameField
                        color: "#b3b3b3"
                        background: Rectangle {
                            color: "#2a2a2a"
                        }
                        placeholderText: "Nickname: User"
                        placeholderTextColor: "#787878"
                        maximumLength: 80
                    }
                    TextField {
                        id: registeredIdField
                        color: "#b3b3b3"
                        background: Rectangle {
                            color: "#2a2a2a"
                        }
                        placeholderText: "Registered ID: User_221957"
                        placeholderTextColor: "#787878"
                        maximumLength: 80
                    }
                    TextField {
                        id: registeredServerField
                        color: "#b3b3b3"
                        background: Rectangle {
                            color: "#2a2a2a"
                        }
                        placeholderText: "Registered Server: accounts.kernel.org"
                        placeholderTextColor: "#787878"
                        maximumLength: 80
                    }
                }
                Image {
                    id: profilePicture
                    width: height
                    anchors.bottom: parent.bottom
                    anchors.margins: parent.height / 10
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    source: "/resources/testPP"
                
                    Rectangle {
                        id: changeProfilePictureMask
                        anchors.fill: parent
                        color: "#320f0f0f"
                        radius: 6
                        visible: false
                
                        Text {
                            id: changeProfilePictureText
                            text: "Change\nImage"
                            color: "#b3b3b3"
                            visible: false
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            anchors.fill: parent
                        }
        
                        MouseArea {
                            id: profilePictureMouseArea
                            hoverEnabled: true
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
        
                            Connections {
                                target: profilePictureMouseArea
                                function onClicked() {
                                    //TODO: Open dialog for choosing Image
                                    console.log("Open file selector dialogue: Not implemented yet")
                                }
                                function onEntered() {
                                    changeprofilePictureImage.visible=true
                                    changeprofilePictureMask.visible=true
                                }
                                function onExited() {
                                    changeprofilePictureImage.visible=false
                                    changeprofilePictureMask.visible=false
                                }
                            }
                        }
                    }
                }
            }
        }
        ScrollView {
            id: audioVideoSettingsScrollView
            width: background.width            
        }
        ScrollView {
            id: appereanceSettingsScrollView
            width: background.width
        }
    }
}
