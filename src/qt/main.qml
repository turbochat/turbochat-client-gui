import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts

import libcall


ApplicationWindow {
    id: window 
    width: 1200
    height: 620
    minimumWidth: 700
    minimumHeight: 400
    visible: true
    color: "#343434"
    title: qsTr("Turbochat")

    Libcall {
        id: libcall
    }

    AddContactPopup {
        id: addContactPopup
    }
    SettingsPopup {
        id: settingsPopup
    }
    
    Rectangle {
        id: contactsContainer
        width: 260
        height: 480
        color: "#2a2a2a"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        Rectangle {
            id: serverButtonContainer
            height: 40
            color: "#1c1c1c"
            anchors.left: parent.left
            anchors.right: parent.horizontalCenter
            anchors.top: parent.top

            MouseArea {
                id: serverButtonMouseArea
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor

                Connections {
                    target: serverButtonMouseArea
                    function onClicked() {
                        privateChatButtonContainer.color = "#1c1c1c";
                        serverButtonContainer.color = "#2a2a2a";
                        contactsSwipeView.setCurrentIndex(0);
                        addContactPopup.setSelectedMode(true);
                    }
                }

                Text {
                    id: serverButtonText
                    color: "#adadad"
                    text: qsTr("Server")
                    font.pixelSize: 13
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }

        Rectangle {
            id: privateChatButtonContainer
            height: 40
            color: "#2a2a2a"
            anchors.left: parent.horizontalCenter
            anchors.right: parent.right
            anchors.top: parent.top

            MouseArea {
                id: privateChatButtonMouseArea
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor

                Connections {
                    target: privateChatButtonMouseArea
                    function onClicked() {
                        privateChatButtonContainer.color = "#2a2a2a";
                        serverButtonContainer.color = "#1c1c1c";
                        contactsSwipeView.setCurrentIndex(1);
                        addContactPopup.setSelectedMode(false);
                    }
                }

                Text {
                    id: privateChatButtonText
                    color: "#adadad"
                    text: qsTr("Private Chats")
                    font.pixelSize: 13
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }

        SwipeView {
            id: contactsSwipeView
            currentIndex: 1
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: privateChatButtonContainer.bottom
            anchors.bottom: addContactButtonContainer.top
            anchors.bottomMargin: 10
            interactive: false
            clip: true
            
            Connections {
                target: libcall
                function onAddContactCard (id, isServer, contactId, contactName, contactPictureSource) {
                    if(isServer) {
                        const component = Qt.createComponent("ServerContactCard.qml");    //Creates a new ContactCard object from the ServerContactCard.qml file and sets its parameters
                        const contactCard = component.createObject(serverColumn, {id: id, contactId: contactId, contactName: contactName, contactPictureSource: contactPictureSource});
                    }
                    else {
                        const component = Qt.createComponent("PrivateContactCard.qml");    //Creates a new ContactCard object from the PrivateContactCard.qml file and sets its parameters
                        const contactCard = component.createObject(privateChatColumn, {id: id, contactId: contactId, contactName: contactName, contactPictureSource: contactPictureSource});
                    }
                }
            }

            ScrollView {
                id: serverScrollView
                width: 260
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                ScrollBar.horizontal.interactive: false
                
                Column {
                    id: serverColumn
                    width: 260
                    padding: 10
                    spacing: 6
    
                    //------ Server Contact Cards will be dynamically created and "pasted" here ------//                
                    
                }
            }
            
            ScrollView {
                id: privateChatScrollView
                width: 260
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                ScrollBar.horizontal.interactive: false

                Column {
                    id: privateChatColumn
                    width: 260
                    spacing: 6
                    padding: 10
                    
                    //------ Private Contact Cards will be dynamically created and "pasted" here ------//
                    
    
                    // This Rectangle "testButtonContainer" and all its child Objects are just for debugging and will be removed as soon as the basic functionality is implemented!
                    
                    Rectangle {
                        id: testButtonContainer
                        width: 240
                        height: 55
                        color: "#3a3a3a"
                        anchors.horizontalCenter: parent.horizontalCenter
    
                        MouseArea {
                            id: testArea
                            anchors.fill: parent
    
                            Connections {
                                target: testArea
                                function onClicked() {
                                    libcall.testButton();
                                }
                                function onPressed() {
                                    testButtonContainer.color = "#953e3e";
                                }
                                function onReleased() {
                                    testButtonContainer.color = "#3a3a3a";
                                }
                            }
                        }
                    }
                }
            }
        }
        
        Rectangle {
            id: addContactButtonContainer
            width: 45
            height: 45
            color: "#2a2a2a"
            radius: 6
            border.width: 1
            border.color: "#adadad"
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            
            Text {
                id: addServerPlus
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: "+"
                color: "#adadad"
                font.pixelSize: 25
            }
            
            
            MouseArea {
                id: addContactButtonMouseArea
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                
                Connections {
                    target: addContactButtonMouseArea
                    function onClicked() {
                        if(contactsSwipeView.currentIndex === 0) {
                            //Show Add Server Addition Popup
                            addContactPopup.setSelectedMode(true);
                            addContactPopup.open();
                        }
                        else {
                            //Show Add Private Chat Addition Popup
                            addContactPopup.setSelectedMode(false);
                            addContactPopup.open();
                        }
                    }
                    function onEntered() {
                        addContactButtonContainer.color = "#353535"
                    }
                    function onExited() {
                        addContactButtonContainer.color = "#2a2a2a"
                    }
                }
            }
        }
        Rectangle {
            id: settingsButtonContainer
            height: 45
            color: "#2a2a2a"
            radius: 6
            border.width: 1
            border.color: "#adadad"
            anchors.left: addContactButtonContainer.right
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10

            Image {
                id: ownProfilePicture
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: 3
                width: parent.height-6
                anchors.topMargin: 3
                anchors.bottomMargin: 3
                fillMode: Image.PreserveAspectCrop
                source: "resources/testPP"
            }
            Text {
                id: ownName
                text: "testName"
                font.pixelSize: 11
                color: "#adadad"
                anchors.left: ownProfilePicture.right
                anchors.leftMargin: 6
                anchors.topMargin: 5
                anchors.top: parent.top
                anchors.bottom: parent.horzizontalCenter
                anchors.right: parent.right
                elide: Text.ElideRight
            }
            Text {
                id: ownId
                text: "@test-server.fr"
                font.pixelSize: 11
                color: "#adadad"
                anchors.left: ownProfilePicture.right
                anchors.leftMargin: 6
                anchors.bottomMargin: 5
                anchors.bottom: parent.bottom
                anchors.top: parent.horzizontalCenter
                anchors.right: parent.right
                elide: Text.ElideRight
            }


            MouseArea {
                id: settingsButtonMouseArea
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor

                Connections {
                    target: settingsButtonMouseArea
                    function onClicked() {
                        settingsPopup.open()
                    }
                    function onEntered() {
                        settingsButtonContainer.color = "#353535"
                    }
                    function onExited() {
                        settingsButtonContainer.color = "#2a2a2a"
                    }
                }
            }
        }
    }

    ScrollView {
        id: messageScrollView
        anchors.left: contactsContainer.right
        anchors.leftMargin: 30
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.top: parent.top
        anchors.bottom: messageScrollViewMask.top
        anchors.bottomMargin: 30
        ScrollBar.horizontal.interactive: false
        
        
        Column {
            id: messageColumn
            visible: true
            spacing: 6
            width: parent.width
            //------ New Messages will be dynamically created and "pasted" here ------//
        
            Connections {
                target: libcall
                function onAddMessageBubble (id, timestamp, sender, content, right) {
                    const component = Qt.createComponent("Message.qml");    //Creates a new Message object from the Message.qml file and sets its parameters
                    const message = component.createObject(messageColumn, {id: id, timestamp: timestamp, sender: sender, content: content, isSentByUser: true});
                    message.setLeftRight();
                    messageScrollView.ScrollBar.vertical.position = messageScrollView.contentHeight;    //Makes the ScrollView scroll down if a new message is added
                }
            }
        }
    }
    
    Rectangle {                                                                                                                                                                                      
        id: messageScrollViewMask
        anchors.bottom: parent.bottom
        anchors.left: contactsContainer.right
        anchors.right: parent.right
        width: 800
        height: 150
        color: "#343434"
        
        Rectangle {
            id: textInputFieldContainer
            width: parent.width*0.8
            height: 80
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "#2a2a2a"
            radius: 6
            MouseArea {
                id: textInputFieldMouseArea
                anchors.fill: parent
                cursorShape: Qt.IBeamCursor
                focus: false
            
                Connections {
                    target: textInputField
                    function onAccepted() {
                        if(textInputField.text != "") {
                            libcall.sendMessage(textInputField.text);
                            textInputField.clear();
                        }
                    }
                }
                
                TextInput {
                    id: textInputField
                    anchors.fill: parent
                    wrapMode: TextInput.Wrap
                    selectByMouse: true
                    color: "#adadad"
                    padding: 5
                }
            }
        }
    }
}
