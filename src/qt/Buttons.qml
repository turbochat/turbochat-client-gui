import QtQuick

Item {
	component BorderedButton : Rectangle {
		id: container
		border.width: 1
		border.color: "#b3b3b3"
		color: "#505050"
		radius: 6
			
		signal entered()
		signal exited()
		signal clicked()
		property string buttonText: ""
		
		Text {
			anchors.fill: parent
			text: buttonText
			color: "#b3b3b3"
			horizontalAlignment: Text.AlignHCenter
			verticalAlignment: Text.AlignVCenter
		}
		MouseArea {
			id: applySettingsMouseArea
			anchors.fill: parent
			hoverEnabled: true
			cursorShape: Qt.PointingHandCursor
			
			Connections {
				target: applySettingsMouseArea
				function onEntered () {
					container.color="#2b2b2b"
					entered()
				}
				function onExited () {
					container.color="#505050"
					exited()
				}
				function onClicked() {
					clicked()
				}
			}
		}
	}
	component TopBarButton : Rectangle {
		id: topBarButtonContainer
		width: 0
		height: 0
		color: "#505050"
		
		signal clicked()
		property string buttonText: ""
		
        MouseArea {
            id: accountButtonMouseArea
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            Connections {
                target: accountButtonMouseArea
                function onClicked () {
					topBarButtonContainer.color = "#505050"
					clicked()
                }
            }
        }
        Text {
            id: topBarButtonText
            color: "#adadad"
            text: buttonText
            font.pixelSize: 13
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
	}
}
