import QtQuick
import QtQuick.Layouts

Item {
    property string content: ""
    property string sender: ""
    property string id: ""
    property string timestamp: ""
    property bool isSentByUser: true
    
    function setLeftRight() {
        if (isSentByUser) {
            messageContainer.anchors.right = this.right
        }
        else {
            messageContainer.anchors.left = this.left
        }
    }
    
    id: messageItem
    width: messageScrollView.width
    height: messageContent.height + messageSender.height
    
    Rectangle {
        id: messageContainer
        width: Math.max(messageSender.width, messageContent.width)
        height: messageContent.height + messageSender.height
        color: "#343434"
        border.width: 1
        border.color: "#b3b3b3"
        radius: 6
    
        Text {
            id: messageSender
            width: Math.min(messageSenderMetrics.boundingRect.width + 12, messageScrollView.width-40, 600)
            color: "#dddddd"
            text: qsTr(sender + ":")
            font.pixelSize: 14
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            padding: 5
            
            TextMetrics {
                id: messageSenderMetrics
                text: messageSender.text
                font: messageSender.font
            }
        }
    
        Text {
            id: messageContent
            anchors.top: messageSender.bottom
            width: Math.min(messageContentMetrics.boundingRect.width + 12, messageScrollView.width-40, 600)
            color: "#ababab"
            text: content
            font.pixelSize: 12
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            wrapMode: Text.Wrap
            padding: 5
            
            TextMetrics {
                id: messageContentMetrics
                text: messageContent.text
                font: messageContent.font
            }
        }
    }
}
