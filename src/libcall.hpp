#include <QObject>
#include <QtQml/qqml.h>
#include <iostream>


class Libcall : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    void contactCardCallback();

    public:
        Q_INVOKABLE void testButton();
        Q_INVOKABLE void sendMessage(QString message);
        Q_INVOKABLE void changeActiveChat(QString contactID);

    signals:
        void addContactCard(int id, bool isServer, int contactId, QString contactName, QString contactPictureSource);
        void addMessageBubble(int id, QString timestamp, QString sender, QString content, bool right);

};
